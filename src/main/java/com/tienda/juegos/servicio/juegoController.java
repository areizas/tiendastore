package com.tienda.juegos.servicio;

import com.tienda.juegos.negocio.Juego;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

import com.tienda.juegos.servicio.*;
import com.tienda.juegos.negocio.*;

@RestController
@RequestMapping("/juegos")
public class juegoController {

	@Autowired
	private Jservice jservice;  //private DataService dataService;


	@RequestMapping(value="/juegos2", method= RequestMethod.GET)
	public String index() {
		 
		System.out.print("adddsda");
		return ("Dasdas");
		//return this.juegoDAO.findAll();
	}

	@PostMapping("/")
	private int add(@RequestBody Juego juego) {
		return jservice.add(juego);
	}

	@PutMapping
	private int update(@RequestBody Juego juego) {
		return jservice.update(juego);
	}


	@GetMapping("/{id}")
	private Juego get(@PathVariable int id) {
		return jservice.get(id);
	}



	@DeleteMapping("/{id}")
	private int delete(@PathVariable int id) {
		return jservice.delete(id);
	}


	/* Getting a list of resources */
	@GetMapping("/")
	private Collection<Juego> list() {
		return jservice.list();
	}


	/*
	@Autowired
	public Jservice gameservice;  //private DataService dataService;

	@PostMapping("/")
	private int add(@RequestBody Juego juego) {
		return gameservice.add(juego);
	}*/


	/*
	@GetMapping("/")
	private Collection<Juego> index() {


		System.out.println("entre");
		return gameservice.index();
	}*/



}
