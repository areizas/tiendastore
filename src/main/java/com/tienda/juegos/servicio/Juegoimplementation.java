package com.tienda.juegos.servicio;

import java.sql.*;
import java.util.List;


import com.tienda.juegos.negocio.Juego;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import com.tienda.juegos.servicio.Jservice;

@Repository
public class Juegoimplementation implements Jservice{

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public int add(final Juego juego) {
        String query = "INSERT INTO juego(id, name) VALUES(?,?)";

        KeyHolder holder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
                ps.setInt(1, juego.getId());
                ps.setString(2, juego.getName());

                return ps;
            }
        }, holder);

        int newUserId = holder.getKey().intValue();
        return newUserId;
    }

    @Override
    public int update(Juego juego) {
        String query = "UPDATE juego SET name = ? WHERE id = ?";
        int row = jdbcTemplate.update(query, juego.getName(), juego.getId());

        return row;
    }

    @Override
    public Juego get(int id) {
        String query = "SELECT * FROM juego WHERE ID = ?";
        return jdbcTemplate.queryForObject(query, new Object[] { id }, new JuegoMapper());
    }

    @Override
    public List<Juego> list() {
        String query = "SELECT * FROM juego";
        return jdbcTemplate.query(query, new JuegoMapper());

    }

    class JuegoMapper implements RowMapper<Juego> {

        @Override
        public Juego mapRow(ResultSet rs, int rowNum) throws SQLException {
            Juego juego = new Juego();
            juego.setId(rs.getInt("id"));
            juego.setName(rs.getString("name"));

            return juego;
        }

    }

    @Override
    public int delete(int id) {
        String query = "DELETE FROM juego WHERE ID = ?";
        int row = jdbcTemplate.update(query, id);

        return row;
    }

}
