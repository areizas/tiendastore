package com.tienda.juegos.servicio;

import com.tienda.juegos.negocio.Juego;

import java.util.List;

public interface Jservice {
    //public int add(Juego juego);
    public int add(Juego juego);
    //public int update(Badge badge);
    public int update(Juego juego);
    public Juego get(int id);

    //public List<Badge> getTool(String tool);

    //public List<Juego> index();
    public List<Juego> list();

    //public int delete(int id);
    public int delete(int id);
}
